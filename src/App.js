import React from "react";
import "./App.css";
import { chooseLanguage } from "./components/chooseLanguage";
import logo from "./favicon.png";
import { Col, Container, Row, Navbar, Nav } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
function App() {
  return (
    <>
      <Router>
        <Navbar bg="ligth" variant="dark">
          <Navbar.Brand href="/">
            <img src={logo} width="30" height="30" alt="" loading="lazy" />{" "}
            reset
          </Navbar.Brand>
          <Switch>
            <Route exact path="/">
              <Nav className="mr-4">
                <Link to="/Contact">Contact</Link>
              </Nav>
            </Route>
            <Route exact path="/Contact">
              <Nav className="mr-4">
                <Link to="/">Accueil</Link>
              </Nav>
            </Route>
          </Switch>
        </Navbar>
        <Container fluid>
          <h4>Choisissez votre language</h4>
          <Row>
            <Col>
              <Switch>
                <Route component={chooseLanguage} exact path="/" />
                <Route component={chooseLanguage} exact path="/Contact" />
              </Switch>
            </Col>
          </Row>
        </Container>
        {/* <p>Language choisit : {connect(language)}</p> */}
      </Router>
    </>
  );
}

export default App;
