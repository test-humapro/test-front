import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "react-bootstrap/Button";

export const MyButton = (props) => {
  const language = useSelector((state) => state.language);
  const dispatch = useDispatch();
  console.log([language, props.value]);
  if (language === props.value) {
    return (
      <>
        <Button onClick={() => dispatch({ type: true, value: props.value })}>
          {props.text}
        </Button>
        <p>Language choisit : {language}</p>
      </>
    );
  } else {
    return (
      <>
        <Button onClick={() => dispatch({ type: false, value: props.value })}>
          {props.text}
        </Button>
        <p>Language choisit : {language}</p>
      </>
    );
  }
};
