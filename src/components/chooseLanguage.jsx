import React from "react";
import { MyButton } from "./button";
import { Col, Container, Row } from "react-bootstrap";

export const chooseLanguage = () => {
  return (
    <>
      <Container>
        <Row>
          <Col xs={2}></Col>
          <Col>
            <MyButton
              value="french"
              text="Français"
              disable="Aucun language choisit"
            />
          </Col>
          <Col xs={2}></Col>
          <Col>
            <MyButton
              value="english"
              text="Anglais"
              disable="Aucun language choisit"
            />
          </Col>
          <Col xs={2}></Col>
        </Row>
      </Container>
    </>
  );
};
